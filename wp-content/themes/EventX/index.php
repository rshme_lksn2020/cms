<?php get_header() ?>

<?php
    $customizes = get_posts([
            'post_type' => 'customize',
            'numberposts' => 1
    ]);
?>

<main id="main">

    <?php get_sidebar(); ?>

    <section id="news">
        <div class="container">
            <h3 class="sub-heading">
                News
            </h3>
            <div class="content">
                <?php
                    $all_news = get_posts([
                            'post_type' => 'news',
                            'numberposts' => 3,
                            'orderby' => 'date',
                            'order' => 'DESC',
                    ]);
                ?>

                <?php foreach($all_news as $news): ?>
                    <div class="card">
                        <div class="card-body">
                            <h3>
                                <?= $news->post_title ?>
                            </h3>

                            <p>
                                <?= substr($news->post_content, 0, 200)  ?>...
                            </p>

                            <a href="<?= get_the_permalink($news->ID) ?>" class="btn btn-primary">
                                Read more
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <section id="save_date">
        <div class="container">
            <h1>
                Let us remind you when this event will start
            </h1>
            <h4>
                Event will begin at
            </h4>

                <?php foreach($customizes as $customize): ?>
                    <div class="time-start">
                        <?php
                            $date_now = new DateTime();
                            $future = implode('-', array_reverse(explode('/', get_field('save_the_date', $customize->ID))));
                            $date_future = new DateTime($future . ' 00:00:00');
                            $diff = date_diff($date_now, $date_future);
                        ?>

                        <span id="days"><?= $diff->format('%a Days')  ?> |</span>
                        <span id="hours"><?= $diff->format('%h Hours')  ?> |</span>
                        <span id="minutes"><?= $diff->format('%i Minutes')  ?> |</span>
                        <span id="seconds"><?= $diff->format('%s Seconds')  ?></span>
                    </div>
                <?php endforeach; ?>

        </div>
    </section>

    <section id="about_us">
        <div class="container">
            <h3 class="sub-heading">
                About Us
            </h3>

            <div class="content">
                <p>
                    <strong>EventX</strong> is a company beside of event music organizer. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci aliquid asperiores assumenda cum cupiditate dolores expedita illo ipsa natus nisi officiis quo, saepe sit, vel veniam. Deleniti expedita nostrum voluptates. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias asperiores blanditiis cum cumque doloribus eaque eius eligendi illum laborum molestiae, officiis provident quaerat qui reprehenderit rerum sed temporibus ut vitae.
                </p>
            </div>
        </div>
    </section>
</main>

<?php get_footer() ?>