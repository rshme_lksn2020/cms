<aside id="sidebar">
    <div class="collapse">
        <div class="bars">
            <div class="bar"></div>
            <div class="bar"></div>
            <div class="bar"></div>
        </div>
    </div>
<?php if ( is_active_sidebar( 'custom-sidebar' ) ) : ?>
<div class="widget-area">
    <h2 class="text-center">Sponsors</h2>
    <?php dynamic_sidebar( 'custom-sidebar' ); ?>
</div>
<?php endif; ?>
</aside>