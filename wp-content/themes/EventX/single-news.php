<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>EventX</title>
    <?php wp_head() ?>
</head>
<body>

<header id="single-page">
    <div class="container">
        <h1><?= the_title() ?></h1>
    </div>
</header>

<section id="content">
    <div class="container">
        <p>
            <?= the_content(); ?>
        </p>
    </div>
</section>

<?php get_footer() ?>
</body>
</html>