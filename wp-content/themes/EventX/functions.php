<?php

class EventX{
    public function __construct(){
        add_action('wp_enqueue_scripts', array($this, 'resource'));
        add_action('init', array($this, 'init'));
        add_action('widgets_init', array($this, 'widgets'));
    }

    public function resource(){
        wp_enqueue_style('main', get_template_directory_uri() . '/css/main.css', array(), '1.0.0', 'all');
        wp_enqueue_script('jssor', get_template_directory_uri() . '/js/jssor.slider-22.2.16.min.js', array(), '1.0.0', true);
        wp_enqueue_script('main', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0.0', true);
    }

    public function init(){
        register_post_type('artist', [
            'label' => 'Artists',
            'public' => true,
            'menu_position' => 2,
            'supports' => ['thumbnail']
        ]);

        register_post_type('customize', [
            'label' => 'Customize',
            'public' => true,
            'menu_position' => 4,
            'supports' => ['']
        ]);

        register_post_type('news', [
            'label' => 'News',
            'public' => true,
            'menu_position' => 3,
            'supports' => ['title', 'editor']
        ]);

        register_post_type('sponsor', [
            'label' => 'Sponsor',
            'public' => true,
            'menu_position' => 5,
            'supports' => ['title', 'thumbnail']
        ]);

        add_theme_support('post-thumbnails');
    }

    public function widgets(){
        register_sidebar([
            'name' => 'Custom Sidebar',
            'id' => 'custom-sidebar',
            'description' => 'Custom Sidebar EventX'
        ]);
    }
}

$theme = new EventX();

class Sponsor extends WP_Widget{
    public function __construct(){
        parent::__construct('sponsor', 'Sponsor', ['description' => 'Sponsor widget']);
    }

    public function widget($args, $instance){
        ?>
            <div class="sponsors-wrapper">
                <?php $sponsors = get_posts([
                        'post_type' => 'sponsor',
                        'numberposts' => -1
                ]) ?>

                <?php foreach($sponsors as $sponsor) : ?>
                    <div class="sponsor">
                        <img src="<?= get_the_post_thumbnail($sponsor->ID) ?>
                        <h4 class="text-center"><?= $sponsor->post_title ?></h4>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php
    }
}

add_action('widgets_init', 'init_widget');

function init_widget(){
    register_widget('Sponsor');
}