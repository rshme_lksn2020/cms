const $ = jQuery

$(document).ready(function(){
    // start code at here

    /* jssor script */
    jssor_1_slider_init = function() {

        var jssor_1_options = {
            $AutoPlay: true,
            $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
            },
            $ThumbnailNavigatorOptions: {
                $Class: $JssorThumbnailNavigator$,
                $Cols: 4,
                $SpacingX: 4,
                $SpacingY: 4,
                $Orientation: 2,
                $Align: 0
            }
        };

        var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

        /*responsive code begin*/
        /*remove responsive code if you don't want the slider scales while window resizing*/
        function ScaleSlider() {
            var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
            if (refSize) {
                refSize = Math.min(refSize, 810);
                jssor_1_slider.$ScaleWidth(refSize);
            }
            else {
                window.setTimeout(ScaleSlider, 30);
            }
        }
        ScaleSlider();
        $Jssor$.$AddEvent(window, "load", ScaleSlider);
        $Jssor$.$AddEvent(window, "resize", ScaleSlider);
        $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
        /*responsive code end*/
    };
    jssor_1_slider_init();
    /* end jssor script */

    /* custom scripts */
    $('body').on('mouseenter', '.slides img', function(){
        const music = $(`audio#music-${this.parentElement.getAttribute('data-artist')}`)[0]
        music.play()
    });

    $('body').on('mouseleave', '.slides img', function(){
        const music = $(`audio#music-${this.parentElement.getAttribute('data-artist')}`)[0]
        music.pause()
        music.currentTime = 0
    });
    countdown()
})

// countdown
function countdown(){
    let day = parseInt($(`span#days`).html().split(' ')[0])
    let hour = parseInt($(`span#hours`).html().split(' ')[0])
    let minute = parseInt($(`span#minutes`).html().split(' ')[0])
    let second = parseInt($(`span#seconds`).html().split(' ')[0])

    setInterval(() => {
        second -= 1
        if(second === 0){
            minute -= 1
            second = 59
            if(minute === 0){
                hour -= 1;
                minute = 59
                if(hour === 0){
                    day -= 1
                    hour = 23
                    if(day === 0 && hour === 0 && minute === 0 && second === 0){
                        alert('event ongoing')
                    }
                }
            }
        }

        $('span#days').html(`${day} Days |`)
        $('span#hours').html(`${hour} Hours |`)
        $('span#minutes').html(`${minute} Minutes |`)
        $('span#seconds').html(`${second} Seconds`)
    }, 1000)

    /* sidebar scripts */
    $('body').on('click', 'aside#sidebar .collapse', function(){
        $(this).toggleClass('active')
        const sidebar = this.parentElement

        if($(this).hasClass('active')){
            sidebar.style.left = '0'
        }
        else{
            sidebar.style.left = '-31%'
        }
    })
}