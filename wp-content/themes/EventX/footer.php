<footer id="footer">
    <div class="container text-center">
        <div class="subscribe">
            <h3>Subscribe</h3>
            <form action="" id="form">
                <input type="email" name="email" id="email" placeholder="example@example.com">
                <button type="submit" class="btn btn-secondary">
                    Subscribe
                </button>
            </form>
        </div>
       <div class="contact-info">
           <h3>Contact Us</h3>
           <span class="d-block">
               admineventx@event.co.id
           </span>
       </div>

        <div class="social_media">
            <h3>Follow Us</h3>
            <?= do_shortcode('[DISPLAY_ULTIMATE_SOCIAL_ICONS] ') ?>
        </div>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>