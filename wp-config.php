<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cms_eventx' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '8 fkB9n3y[bq+!L5dX.yE`163lD8Q1>I:^~K;9:ZapByA7H+yxh36 u0|gpc3E48' );
define( 'SECURE_AUTH_KEY',  'p~up?;ja;}y&<{p1v>ckzAA(FD,MP}WrYnU2sFl;8rG-noy<u|n6,m@J.>1<JsUN' );
define( 'LOGGED_IN_KEY',    '2)UfT2k41xt-Xh]ri{b`+*8|PJDG>!M~vD%1jt@h(`:T;wrj+#5h8<=m^#1t[m3n' );
define( 'NONCE_KEY',        '+fA!w.eDjZfxcxB>;&@}L]#&%0{n=ghhUs$58$g|F++gmOF#mga4J/@3/1zXjsWO' );
define( 'AUTH_SALT',        '3|fEeIB$;q|oB{q:4*?,&50162MpnwEN>byEGkOm7_fNC8$+X$E`iu@[O>MIl~U:' );
define( 'SECURE_AUTH_SALT', 'ntzIek41Q3s)4dgX/)l_9(:,Cot/`8I&1pKNm1UUTZn;`xOOL==hk7|1T_niQqH.' );
define( 'LOGGED_IN_SALT',   'i6k&eZ_SGf6UxkJ;tNy:#SCFpQ:@CgDXB_S%tQ[&U>YI9ZS[#EUPJsm==},lU5=:' );
define( 'NONCE_SALT',       '}GFoo&D^PF07`[:F}GSq`%FyA1sm,w*Fm4Ktr/t(Zppic[!B{$#sNve?$ 6{p:2j' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
